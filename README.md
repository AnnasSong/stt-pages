# stt-pages


> static web app workflow

## Installation

```shell
$ yarn add stt-pages

# or npm
$ npm install stt-pages
```

## Usage

<!-- TODO: Introduction of API use -->

```javascript
const sttPages = require('stt-pages')
const result = sttPages('stt')
// result => 'stt@stt.me'
```

## API

<!-- TODO: Introduction of API -->

### sttPages(name[, options])

#### name

- Type: `string`
- Details: name string

#### options

##### host

- Type: `string`
- Details: host string
- Default: `'stt.me'`

## Contributing

1. **Fork** it on GitHub!
2. **Clone** the fork to your own machine.
3. **Checkout** your feature branch: `git checkout -b my-awesome-feature`
4. **Commit** your changes to your own branch: `git commit -am 'Add some feature'`
5. **Push** your work back up to your fork: `git push -u origin my-awesome-feature`
6. Submit a **Pull Request** so that we can review your changes.

> **NOTE**: Be sure to merge the latest from "upstream" before making a pull request!

## License

[MIT](LICENSE) &copy; stt




