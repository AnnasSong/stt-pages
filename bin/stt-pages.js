#!/usr/bin/env node
//配置package.json中bin字段，作为cli入口文件  "bin": "bin/stt-pages.js",
//yarn link 到全局
/**
 * yarn build --gulpfile ./node_modules/stt-pages/lib/index.js --cwd .
 * 上面命令可以 指定gulpfile文件，和当前工作目录
 * 
 * 将对gulp的调用和上面传的参数放在本入口文件中自动调用，就可以在项目demo中省略gulpfile.js文件
 */

/**
 * process.argv 属性返回数组，其中包含启动 Node.js 进程时传入的命令行参数。 
 * 第一个元素将是 process.execPath。 
 * 第二个元素将是正在执行的 JavaScript 文件的路径。 其余元素将是任何其他命令行参数。
 */
const path = require('path')
process.argv.push('--cwd')
process.argv.push(process.cwd())
process.argv.push('--gulpfile')
// 指定gulpfile.js文件，在lib/index.js 

//require.resolve函数来查询当前模块文件的带有完整绝对路径的文件名   __dirname也是绝对路径，但是需要拼接
//下面两个路径一致
// process.argv.push(require.resolve('../lib/index.js'))
// process.argv.push(path.join(__dirname,'../lib/index.js'))

//指定stt-pages根目录会自动查找package.json文件下main字段对应路径，因此可以省略/lib/index.js
process.argv.push(require.resolve('..'))
/**
 * gulp入口文件执行代码：
 *  #!/usr/bin/env node
 *  require('gulp-cli')();
 * 
 * 所以我们在这个入口文件中执行gulp入口文件就可以
 */
require('gulp/bin/gulp')

// yarn publish 会发布项目根目录下的文件，还有files字段下面的文件夹，创建项目时没有bin文件夹，这里需要加上
