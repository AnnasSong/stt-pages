const {
    src,
    dest,
    parallel,
    series,
    watch
} = require('gulp')

const del = require('del')
const bs = require('browser-sync')

// 加载gulp下面所有插件，其他插件以Plugins属性形式调用
const loadPlugins = require('gulp-load-plugins')
const Plugins = loadPlugins()
// const Plugins.sass = require('gulp-sass')(require('sass'))
// const Plugins.babel = require('gulp-babel')
// const swig = require('gulp-swig')
// const Plugins.imagemin = require('gulp-imagemin')

const path = require('path')
// 抽离data
const cwd = process.cwd() // 当前工作目录
// 路径可配置
let config = {
  //default
    build:{
        src:'src',
        dist:'dist',
        temp:'temp',
        public:'public',
        paths:{
            styles:'assets/styles/*.scss',
            scripts:'assets/scripts/*.js',
            pages:'*.html',
            images:'assets/images/**',
            fonts:'assets/fonts/**'
        }
    }
}

try {
    const loadConfig = require(path.join(cwd, '/pages.config.js'))
    config = Object.assign({}, config, loadConfig)
} catch (error) {}
// 样式编译
const style = () => {
    return src(config.build.paths.styles, {
        base: config.build.src,
        cwd:config.build.src
    })
    .pipe(Plugins.sass())
    .pipe(dest(config.build.temp))
    //构建优化
    .pipe(bs.reload({
        stream: true
    }))
}
// 脚本编译
const script = () => {
    return src(config.build.paths.scripts, {
        base: config.build.src,
        cwd:config.build.src
    })
    // babel是ES转换平台，起作用的是平台里的插件，使用babel需要传入插件 preset是插件的集合 preset-env是所有特性的集合
    .pipe(Plugins.babel({
        // require载入，先从当前文件所在文件夹找，找不到会继续往上一级文件夹下找
        presets: [require('@babel/preset-env')]
    }))
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({
        stream: true
    }))
}

//页面模板编译
const page = () => {
    console.log('8888')
    return src(config.build.paths.pages, {
        base: config.build.src,
        cwd:config.build.src
    })
    .pipe(Plugins.swig({
        data: config.data
    }))
    .pipe(dest(config.build.temp))
    .pipe(bs.reload({
        stream: true
    }))
}
// 图片压缩
const img = () => {
    return src(config.build.paths.images, {
        base: config.build.src,
        cwd:config.build.src
    })
    .pipe(Plugins.imagemin())
    .pipe(dest(config.build.dist))
}
// 图标压缩
const fonts = () => {
    return src(config.build.paths.fonts, {
        base: config.build.src,
        cwd:config.build.src
    })
    .pipe(Plugins.imagemin())
    .pipe(dest(config.build.dist))
}
//其他文件打包
const extra = () => {
    return src('**', {
        base: config.build.public,
        cwd:config.build.public,
    })
    .pipe(dest(config.build.dist))
}
// 文件清除
const clean = () => {
  // del 是个promise
    return del([config.build.dist, config.build.temp])
}
const serve = () => {
  //监听文件，变化时执行对应的任务
    watch(config.build.paths.styles, {
        base: config.build.src,
        cwd:config.build.src
    }, style)
    watch(config.build.paths.scripts,{
        base: config.build.src,
        cwd:config.build.src
    }, script)
    watch(config.build.paths.pages, {
        base: config.build.src,
        cwd: config.build.src
    }, page)
    watch([config.build.paths.images, config.build.paths.fonts],{
        base: config.build.src,
        cwd:config.build.src
    }, bs.reload) 
    watch('**', {
        base: config.build.public,
        cwd:config.build.public
    },bs.reload)
    bs.init({
    port: 2080,
    notify: false,
    //是都自动打开浏览器
    // open:false,
    // 监听文件路径，当文件修改时热更新，编译时就reload了，所以这里不需要监听编译后的temp文件夹
    // files:'temp/**',
    server: {
      //根目录 文件修改打包到temp目录
        baseDir: [config.build.temp, config.build.src, config.build.public],
        //优先于baseDir  本地项目路由
        routes: {
            '/node_modules': 'node_modules'
        }
    }
    })
}
// gulp-useref文件引用处理
/**
 * <!-- build:css assets/styles/vendor.css -->
 * <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.css">
 * <!-- endbuild -->
 */

const useref = () => {
    return src(config.build.paths.pages, {
        base: config.build.temp,
        cwd:config.build.temp
    })
    // 处理文件引用
    .pipe(Plugins.useref({
        searchPath: [config.build.temp, '.']
    }))
    // 文件压缩 js css html
    .pipe(Plugins.if(/\.js$/, Plugins.uglify())) //js
    .pipe(Plugins.if(/\.css$/, Plugins.cleanCss())) //css
    .pipe(Plugins.if(/\.html$/, Plugins.htmlmin({ //html
      collapseWhitespace: true, //压缩空格回车
      minifyCSS: true, //压缩html文件中css代码
      minifyJS: true, //压缩html文件中js代码
    }))) //html
    // 文件一边读取一边写入，会有冲突，造成错误，读写文件夹分开
    .pipe(dest(config.build.dist))
}

//并行执行任务 编译js css html文件
/**
 *   js  es5=>es6
 *  css  scss=>css
 * html  模板编译<%= name %>
 */
const compile = parallel(style, script, page)
//构建优化 清除之前的包=>编译js/css/html=>压缩js/css/html和其他文件（图片/图标等）=>打包导出到指定文件夹
const build = series(clean, parallel(compile, img, fonts, extra), useref)
// 开发环境 编译js/css/html=>监听文件=>启动服务器
const develop = series(compile, serve)
module.exports = {
    build,
    clean,
    develop,
}